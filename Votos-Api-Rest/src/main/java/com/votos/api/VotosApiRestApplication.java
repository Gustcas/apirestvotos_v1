package com.votos.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VotosApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(VotosApiRestApplication.class, args);
	}

}
