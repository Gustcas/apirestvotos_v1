package com.votos.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.votos.api.entitys.votos;

public interface votosDAO  extends JpaRepository<votos, Long> {
	
	@Query(value = " select * from votos\r\n" + 
			"where  MONTH(fecha_voto) = MONTH(sysdate()) \r\n" + 
			"AND id_empleado in (select id_empleado from votos where voto>0 )\r\n" + 
			"group by id_empleado ", nativeQuery = true)
		List<votos> finVotosMesEmpleado();
		
	
		
}
