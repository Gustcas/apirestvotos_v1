package com.votos.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.votos.api.entitys.emplado;
import com.votos.api.entitys.votos;

public interface empladoDAO extends JpaRepository<emplado,Long> {
	
	@Query(value = "select e.* ,count(*) CantidadempleadosRegistrados from emplado e", nativeQuery = true)
	List<emplado> fincountEmpladosResgistrados();
}
