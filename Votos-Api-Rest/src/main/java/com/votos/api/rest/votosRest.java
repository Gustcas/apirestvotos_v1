package com.votos.api.rest;

import java.security.cert.PKIXRevocationChecker.Option;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.ls.LSInput;

import com.votos.api.dao.deapartementoDAO;
import com.votos.api.dao.votosDAO;
import com.votos.api.entitys.deapartemento;
import com.votos.api.entitys.votos;

@RestController
@RequestMapping("votos")
public class votosRest {
	
	@Autowired
	private votosDAO votodao;	

	
	@GetMapping
	public ResponseEntity<List<votos>> getvotos(){
		List<votos> votos = votodao.findAll();
		return ResponseEntity.ok(votos);
	}
	
	@RequestMapping(value = "{id_voto}") // votos/{id_voto}
	public ResponseEntity<votos> getvotosById_voto(@PathVariable("id_voto") Long id_voto){
	   Optional<votos> Optionalvoto =  votodao.findById(id_voto);
	   if (Optionalvoto.isPresent()) {
		   return ResponseEntity.ok(Optionalvoto.get());		
	}else {
		return ResponseEntity.noContent().build();
		}
	}
	
	@PostMapping /// post
	public ResponseEntity<votos> createVoto (@RequestBody votos Voto){
	 votos VotoNew = votodao.save(Voto);
	 return ResponseEntity.ok(VotoNew);
	}
	
	
	@DeleteMapping(value = "{id_voto}") /// delete
	public ResponseEntity<Void> deleteVoto (@PathVariable("id_voto") Long id_voto){
	 votodao.deleteById(id_voto);
	 return ResponseEntity.ok(null);
	}
	
	
	@PutMapping
	public ResponseEntity<votos> updateVoto (@RequestBody votos Voto){
		System.out.println("This is an informational message");
		Optional<votos> OptionalupdateVoto = votodao.findById(Voto.getId_voto());
		if(OptionalupdateVoto.isPresent()) {
			votos updateVoto = OptionalupdateVoto.get();
			updateVoto.setId_empleado(Voto.getId_departamento());
			updateVoto.setId_departamento(Voto.getId_departamento());
			updateVoto.setVoto(Voto.getVoto());
			updateVoto.setComentario(Voto.getComentario());
			updateVoto.setFecha_voto(Voto.getFecha_voto());
			updateVoto.setEstado_voto(Voto.getEstado_voto());
			votodao.save(updateVoto);
			return ResponseEntity.ok(updateVoto);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@RequestMapping(value = "/EmpleadoVotadosMes")
	@GetMapping
	public ResponseEntity<List<votos>> getEmpleadoVostoMes(){
		List<votos> votos = votodao.finVotosMesEmpleado();
		return ResponseEntity.ok(votos);
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
