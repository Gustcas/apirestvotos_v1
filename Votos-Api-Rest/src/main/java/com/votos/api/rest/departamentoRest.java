package com.votos.api.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.votos.api.dao.deapartementoDAO;
import com.votos.api.entitys.deapartemento;
@RestController
@RequestMapping("departamento")
public class departamentoRest {
	@Autowired
	private deapartementoDAO deapartementodao;	
	
	@GetMapping
	public ResponseEntity<List<deapartemento>> getdepartamentos(){
		List<deapartemento> deapartemento = deapartementodao.findAll();
		return ResponseEntity.ok(deapartemento);
	}

	@RequestMapping(value = "{id_departamento}") // deapartemento/{id_departamento}
	public ResponseEntity<deapartemento> getdepartamentoById_departamento(@PathVariable("id_departamento") Long id_departamento){
	   Optional<deapartemento> Optionaldepartamento =  deapartementodao.findById(id_departamento);
	   if (Optionaldepartamento.isPresent()) {
		   return ResponseEntity.ok(Optionaldepartamento.get());		
	}else {
		return ResponseEntity.noContent().build();
		}
	}
	
	@PostMapping /// post
	public ResponseEntity<deapartemento> createDepartamento (@RequestBody deapartemento Deapartemento){
	deapartemento DepartamentoNew = deapartementodao.save(Deapartemento);
	 return ResponseEntity.ok(DepartamentoNew);
	}
	
	
	@DeleteMapping(value = "{id_departamento}") /// delete
	public ResponseEntity<Void> deapartementodao (@PathVariable("id_departamento") Long id_departamento){
		deapartementodao.deleteById(id_departamento);
	 return ResponseEntity.ok(null);
	}
	
	
	@PutMapping
	public ResponseEntity<deapartemento> updateDepartamento (@RequestBody deapartemento Deapartemento){
		Optional<deapartemento> OptionalupdateDepartamento = deapartementodao.findById(Deapartemento.getId_departamento());
		if(OptionalupdateDepartamento.isPresent()) {
			deapartemento updateDepartamento= OptionalupdateDepartamento.get();
			updateDepartamento.setId_departamento(Deapartemento.getId_departamento());
			updateDepartamento.setDepartamneto(Deapartemento.getDepartamneto());		
			deapartementodao.save(updateDepartamento);
			return ResponseEntity.ok(updateDepartamento);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

}
