package com.votos.api.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.votos.api.dao.deapartementoDAO;
import com.votos.api.dao.empladoDAO;
import com.votos.api.entitys.deapartemento;
import com.votos.api.entitys.emplado;
import com.votos.api.entitys.votos;

@RestController
@RequestMapping("emplado")
public class empladoRest {
	
	@Autowired
	private empladoDAO empladodao;	

	
	@GetMapping
	public ResponseEntity<List<emplado>> getemplados(){
		List<emplado> emplado = empladodao.findAll();
		return ResponseEntity.ok(emplado);
	}

	@RequestMapping(value = "{id_empleado}") // deapartemento/{id_departamento}
	public ResponseEntity<emplado> getempladoById_emplado(@PathVariable("id_empleado") Long id_empleado){
	   Optional<emplado> Optionalemplado=  empladodao.findById(id_empleado);
	   if (Optionalemplado.isPresent()) {
		   return ResponseEntity.ok(Optionalemplado.get());		
	}else {
		return ResponseEntity.noContent().build();
		}
	}
	
	@PostMapping /// post
	public ResponseEntity<emplado> createEmplado (@RequestBody emplado Emplado){
		emplado EmpladoNew = empladodao.save(Emplado);
	 return ResponseEntity.ok(EmpladoNew);
	}
	
	
	@DeleteMapping(value = "{id_empleado}") /// delete
	public ResponseEntity<Void> Empladodao (@PathVariable("id_empleado") Long id_empleado){
		empladodao.deleteById(id_empleado);
	 return ResponseEntity.ok(null);
	}
	
	
	@PutMapping
	public ResponseEntity<emplado> updateEmplado(@RequestBody emplado Emplado){
		Optional<emplado> OptionalupdateEmplado = empladodao.findById(Emplado.getId_empleado());
		if(OptionalupdateEmplado.isPresent()) {
			emplado updateEmplado = OptionalupdateEmplado.get();
			updateEmplado.setId_empleado(Emplado.getId_empleado());
			updateEmplado.setNombre_empledo(Emplado.getNombre_empledo());		
			empladodao.save(updateEmplado);
			return ResponseEntity.ok(updateEmplado);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@RequestMapping(value = "/CountEmpleadosRegistrados")
	@GetMapping
	public ResponseEntity<List<emplado>> getCountEmpleadosRegitrados(){
		List<emplado> emplado = empladodao.fincountEmpladosResgistrados();
		return ResponseEntity.ok(emplado);
	}
	
	
	
	
	
}
