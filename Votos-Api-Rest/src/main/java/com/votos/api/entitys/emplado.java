package com.votos.api.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="emplado")
public class emplado {
	
	public Long getId_empleado() {
		return id_empleado;
	}
	public void setId_empleado(Long id_empleado) {
		this.id_empleado = id_empleado;
	}
	public String getNombre_empledo() {
		return nombre_empledo;
	}
	public void setNombre_empledo(String nombre_empledo) {
		this.nombre_empledo = nombre_empledo;
	}
	@Id
	@Column(name="id_empleado")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_empleado;	
	@Column(name="nombre_empledo" ,length = 120)
	private String nombre_empledo;
}
