package com.votos.api.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deapartemento")
public class deapartemento {
	
	public Long getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(Long id_departamento) {
		this.id_departamento = id_departamento;
	}
	public String getDepartamneto() {
		return departamneto;
	}
	public void setDepartamneto(String departamneto) {
		this.departamneto = departamneto;
	}
	@Id
	@Column(name="id_departamento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_departamento;	
	@Column(name="departamneto" ,length = 120)
	private String departamneto;
	
}
