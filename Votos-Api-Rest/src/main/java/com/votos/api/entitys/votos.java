package com.votos.api.entitys;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="votos")

public class votos {

	public Long getId_voto() {
		return id_voto;
	}
	public void setId_voto(Long id_voto) {
		this.id_voto = id_voto;
	}
	public int getId_empleado() {
		return id_empleado;
	}
	public void setId_empleado(int id_empleado) {
		this.id_empleado = id_empleado;
	}
	public int getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}
	public int getVoto() {
		return voto;
	}
	public void setVoto(int voto) {
		this.voto = voto;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Date getFecha_voto() {
		return fecha_voto;
	}
	public void setFecha_voto(Date fecha_voto) {
		this.fecha_voto = fecha_voto;
	}
	public boolean getEstado_voto() {
		return estado_voto;
	}
	public void setEstado_voto(boolean estado_voto) {
		this.estado_voto = estado_voto;
	}
	@Id
	@Column(name="id_voto")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_voto;	
	
	@Column(name="id_empleado")
	private int id_empleado;
	@Column(name="id_departamento")
	private int id_departamento;
	@Column(name="voto" , nullable = false, length = 1000)
	private int voto;	
	@Column(name="comentario" ,length = 120)
	private String comentario;
	@Column(name="fecha_voto")
	private Date fecha_voto;
	@Column(name="estado_voto")
	private boolean estado_voto;
	
}
